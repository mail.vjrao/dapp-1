import os
import json
import hashlib
import asyncio
from decimal import Decimal
from typing import List, Dict, Any, AsyncGenerator
from functools import cmp_to_key



# Exceptions and Encoders
class InvalidTransactionException(Exception):
    """Exception raised for invalid transactions."""
    pass


class DecimalEncoder(json.JSONEncoder):
    """JSON Encoder for Decimal objects."""
    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        return super(DecimalEncoder, self).default(obj)


# Interfaces
class OutputHandler:
    """ Interface for output handling. """
    def save_block(self, block_number: int, data: str) -> None:
        pass

    def save_state(self, file_name: str, account_state: Dict[str, Any]) -> None:
        pass

    def log(self, message: str) -> None:
        pass

    def error(self, message: str) -> None:
        pass


# Implementation of OutputHandler to the filesystem
class FileOutputHandler(OutputHandler):
    """ File implementation of OutputHandler. """
    def __init__(self, output_path: str):
        self.output_path = output_path

    def save_block(self, block_number: int, data: str) -> None:
        block_file_path = os.path.join(self.output_path, f"block{block_number}.txt")
        with open(block_file_path, 'w') as block_file:
            block_file.write(data)

    def save_state(self, file_name: str, account_state: Dict[str, Any]) -> None:
        with open(file_name, 'w') as file:
            json.dump(account_state, file, cls=DecimalEncoder, indent=4)

    def log(self, message: str) -> None:
        # Just print for now, could be written to a log file
        print(message)

    def error(self, message: str) -> None:
        # Just print for now, could be written to a log file
        print(f"ERROR: {message}")




class TransferTransaction:
    def __init__(self, from_account: str, to_account: str, amount: Decimal, gas_fee: Decimal):
        self.from_account = from_account
        self.to_account = to_account
        self.amount = amount
        self.gas_fee = gas_fee

    def to_string(self) -> str:
        return f"transfer | {self.from_account} | {self.to_account} | {self.amount} | {self.gas_fee}"

class CoinbaseTransaction:
    def __init__(self, to_account: str, bonus: Decimal = Decimal('5')):
        self.to_account = to_account
        self.bonus = bonus

    def to_string(self) -> str:
        return f"coinbase | {self.to_account}"


class AccountState:
    state_map = {}
    @staticmethod
    def get_mining_reward():
        return Decimal(5)

    def __init__(self):
        self.state_map = {}

    def account_init(self, account:str):
        if account not in self.state_map:
            self.state_map[account] = { 'balance': Decimal(0) }
        return self.state_map[account]

    def set_account_balance(self, account, balance : Decimal):
        self.state_map[account] = { 'balance': balance }
        return self.state_map[account]

    def earn_mining_reward(self, miner_account):
        self.account_init(miner_account)
        self.state_map[miner_account]['balance'] += AccountState.get_mining_reward()

    def _transfer_to(self, account, amount):
        self.account_init(account)
        self.state_map[account]['balance'] += Decimal(amount)

    def _transfer_from(self, account, amount):
        self.account_init(account)
        self.state_map[account]['balance'] -= Decimal(amount)

    def _pay_fee(self, account, fee):
        self.account_init(account)
        self.state_map[account]['balance'] -= Decimal(fee)

    def apply_transaction(self, txn:TransferTransaction, miner_account: str):
        self._transfer_from(txn.from_account, txn.amount)
        self._transfer_to(txn.to_account, txn.amount)
        self._pay_fee(miner_account, txn.gas_fee)
        return (self.state_map[miner_account], self.state_map[txn.from_account],
                self.state_map[txn.to_account])




# Block class
class Block:

    block_address = []
    prev_block_hash = ""
    current_hash = ""
    block_number = 0
    transactions = []

    def __init__(self,
                 block_address:str,
                 prev_hash: str,
                 transactions: List[TransferTransaction],
                 block_number: int):
        self.prev_block_hash = prev_hash
        self.block_address = block_address
        self.current_hash = block_address
        self.transactions = transactions
        self.block_number = block_number
        self.current_hash = self.calculate_hash()

    def calculate_hash(self) -> str:
        block_content = '\n'.join([txn.to_string() for txn in self.transactions])
        full_content = f"{self.prev_block_hash}\n{block_content}".encode()
        return hashlib.sha256(full_content).hexdigest()

    def save_to_file(self, handler: OutputHandler) -> None:
        block_data = f"{self.prev_block_hash}\n"
        block_data += '\n'.join([txn.to_string() for txn in self.transactions]) + '\n'
        block_data += self.current_hash
        handler.save_block(self.block_number, block_data)


# Miner class
class Miner:

    # these represent the blocks in a blockchain
    mined_blocks = List[Block]

    @staticmethod
    def sort_fn (tx1:TransferTransaction, tx2:TransferTransaction) -> int:
        if tx1.gas_fee > tx2.gas_fee:
            return 1
        else:
            return -1

    def __init__(self, state: AccountState, output_handler: OutputHandler):
        self.state = state
        self.output_handler = output_handler
        self.previous_hash = '0' * 64
        self.mined_blocks = []
        self.temp_transactions = []
        self.temp_block_number = 0

    def process_transaction(self, transaction_str: str) -> TransferTransaction:
        txn_type, *args = transaction_str.strip().split(' | ')
        if transaction_str.startswith('transfer'):
            from_acc, to_acc, amount, fee = args
            amount, fee = Decimal(amount), Decimal(fee)
            
            txn = TransferTransaction(from_acc, to_acc, amount, fee)
            self.temp_transactions.append(txn)

        elif transaction_str.startswith('coinbase'):
            recipient = args[0]

            # sort transactions in fee order.
            self.temp_transactions.sort(key=cmp_to_key(Miner.sort_fn), reverse=True)
            new_block = Block(prev_hash=self.previous_hash,
                              block_address=recipient,
                              transactions=self.temp_transactions,
                              block_number=self.temp_block_number)
            self.mined_blocks.append(new_block)

            # prepare for the next mined block
            self.previous_hash = new_block.current_hash
            self.temp_block_number = self.temp_block_number + 1
            self.temp_transactions = []
        else:
            raise InvalidTransactionException(f"Unknown transaction type: {transaction_str}")

    def mine_blocks(self, transactions_file: str) -> None:
        with open(transactions_file, 'r') as file:
            raw_transactions = [line.strip() for line in file]

            for raw_txn in raw_transactions:
                self.process_transaction(raw_txn)

    def compute_state(self):
        account_state = AccountState()
        for block in self.mined_blocks:
            miner_account = block.block_address

            account_state.earn_mining_reward(miner_account)
            for txn in block.transactions:
                account_state.apply_transaction(txn, miner_account)
        return account_state

    def output_state(self):
        account_state = self.compute_state()


# Example Usage
if __name__ == '__main__':
    os.makedirs('./blocks', exist_ok=True)  # Create a directory for block files if it doesn't exist
    state = AccountState()
    file_handler = FileOutputHandler('./blocks')  # Pass the directory path where block files will be saved
    miner = Miner(state, file_handler)
    miner.mine_blocks('transactions.txt')  # Ensure that 'transactions.txt' is in the same directory as the script
    state = miner.compute_state()
    for acct in state.state_map.keys():
        print(f"{acct} - {state.state_map[acct]}")
